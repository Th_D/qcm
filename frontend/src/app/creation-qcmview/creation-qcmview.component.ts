import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QcmService } from '../services/qcm.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-creation-qcmview',
  templateUrl: './creation-qcmview.component.html',
  styleUrls: ['./creation-qcmview.component.css']
})
export class CreationQCMViewComponent implements OnInit {
/*
  constructor() { }

  ngOnInit() {
  }

  addQuestion()
  {
    let questionBaseClass = document.getElementsByClassName("questionBase");
    let componentQuestion = document.createElement("app-qcm-question-element");
    componentQuestion.innerHTML = questionBaseClass[0].innerHTML;
    document.getElementById('content_question').appendChild(componentQuestion);
  }
*/

  @Input() nameQcm:string;
  @Input() descQcm:string;
  tabData:any[];

  idQcm:number;
  idQuestion:number;
  orderForm: FormGroup;
  items: FormArray;

  constructor(private formBuilder: FormBuilder, private qcmService:QcmService, private router:Router) {}

  ngOnInit() {
    this.orderForm = this.formBuilder.group({
      items: this.formBuilder.array([ this.createItem() ])
    });
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      enonce: '',
      choix1: '',
      isValid1: false,
      choix2:'',
      isValid2:false,
      choix3:'',
      isValid3:false
    });
  }

  addItem(): void {
    this.items = this.orderForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }


  submitQcm()
  {

    this.qcmService.addQcm(this.nameQcm, this.descQcm).subscribe(
      (result) => {
        this.idQcm = +JSON.stringify(result['idQcm']);
        this.tabData = this.orderForm['value']['items'];
        for(let value of this.tabData)
        {
          console.log(value);
          this.qcmService.addQuestionToQcm(this.idQcm, value['enonce']).subscribe(
            (result) => {
              this.idQuestion = +JSON.stringify(result['idQuestion']);
              for(let i=0;i<3;i++){
                this.qcmService.addChoiceToQuestion(this.idQcm, this.idQuestion,value['choix'+(i+1)], value['isValid'+(i+1)]?1:0).subscribe(
                  (result) => {
                    console.log("YOUHOU");
                  },
                  (err) => {
                    console.log(err);
                  }
                );
              }
            },
            (err) => {
              console.log(err);
            }
          );
        }
      },
      (err) => {
        console.log(err);

      }

    );

    this.router.navigate(['professor/gestionQCM']);
  }


}
