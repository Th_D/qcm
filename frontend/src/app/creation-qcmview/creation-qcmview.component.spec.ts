import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationQCMViewComponent } from './creation-qcmview.component';

describe('CreationQCMViewComponent', () => {
  let component: CreationQCMViewComponent;
  let fixture: ComponentFixture<CreationQCMViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationQCMViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationQCMViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
