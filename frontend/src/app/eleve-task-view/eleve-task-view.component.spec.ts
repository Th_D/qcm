import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EleveTaskViewComponent } from './eleve-task-view.component';

describe('EleveTaskViewComponent', () => {
  let component: EleveTaskViewComponent;
  let fixture: ComponentFixture<EleveTaskViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EleveTaskViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EleveTaskViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
