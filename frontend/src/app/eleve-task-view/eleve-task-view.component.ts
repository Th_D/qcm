import { Component, OnInit } from '@angular/core';
import { EleveService } from "../services/eleve.service";
import { EpreuvesViewComponent } from '../epreuves-view/epreuves-view.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalAnswerComponent } from "../modal-answer/modal-answer.component";


@Component({
  selector: 'app-eleve-task-view',
  templateUrl: './eleve-task-view.component.html',
  styleUrls: ['./eleve-task-view.component.css']
})
export class EleveTaskViewComponent implements OnInit {
    bsModalRef: BsModalRef;

    public QcmList = [];
    public questionsList = [];

  constructor(private eleveService: EleveService, private modalService: BsModalService) {
    this.subscriptions(eleveService);
  }

  ngOnInit() {
  }

  subscriptions(eleveService:EleveService){
      this.eleveService.getQcmsDispo(this.toTimestamp(new Date())).subscribe(
        (qcmListFromAPI) => {
          this.QcmList = qcmListFromAPI['epreuves'];
        },
        (err) => {
          console.log(err);
        }
    );

      setTimeout(function(eleveService){
          this.subscriptions(this.eleveService);
      }.bind(this),1000);
  }

  openModal(qcm:string, date:string, idExam:string, idUser:string) {
    this.getQCM(idExam, function(){
        const initialState = {
          title: ''+qcm+' | '+date,
          questionsList: this.questionsList,
          idExam:idExam,
          idUser:idUser
        };
        this.bsModalRef = this.modalService.show(ModalAnswerComponent, {initialState});
        this.bsModalRef.content.closeBtnName = 'Envoyer';
    }.bind(this));
}

    getQCM(idExam:string, callback){
        this.eleveService.getQuestions(idExam).subscribe(
          (questionsListFromAPI) => {
              this.questionsList = this.parseQuestionsList(questionsListFromAPI['questions']);
              callback();
          },
          (err) => {
            console.log("error : " + err);
          }
        );
    }

    parseQuestionsList(questionsListFromAPI:any[]){
        let questionsList = [];
        for(let i=0; i<questionsListFromAPI.length; i++){
            if(i%3==0){
                let question = {
                    id:"",
                    enonce:"",
                    choix:[]
                };

                question.id = questionsListFromAPI[i]['idQuestion'];
                question.enonce = questionsListFromAPI[i]['enonce'];
                question.choix = [];

                questionsList.push(question);
            }

            let choix = {
                choice:"",
                idChoice:"",
            }
            choix.choice = questionsListFromAPI[i]['choice'];
            choix.idChoice = questionsListFromAPI[i]['idChoice'];

            questionsList[questionsList.length-1].choix.push(choix);
        }
        console.log(questionsList);
        return questionsList;
    }

  toTimestamp(date: Date) {
    var annee = date.getFullYear();
    var mois = date.getMonth() + 1;
    var jour = date.getDate();
    var heure = date.getHours();
    var minute = date.getMinutes();
    var seconde = date.getSeconds();
    return '' + annee + '-' + mois + '-' + jour + ' ' + heure + ':' + minute + ':' + seconde;
  }

  toDate(dateT:string){
      var date = new Date(dateT);
      var annee   = date.getFullYear();
      var mois    = ('0'+(date.getMonth() + 1)).slice(-2);
      var jour    = ('0'+date.getDate()).slice(-2);
      var heure   = ('0'+date.getHours()).slice(-2);
      var minute  = ('0'+date.getMinutes()).slice(-2);
      var seconde = ('0'+date.getSeconds()).slice(-2);
      return ''+jour+'/'+mois+'/'+annee+' '+heure+':'+minute+':'+seconde;
  }

  toEnd(date:string){
      var now = +new Date();
      var tmp = +new Date(date) - now;

      tmp = Math.floor(tmp/1000);
      var sec = tmp % 60;

      tmp = Math.floor((+tmp-sec)/60);
      var min = tmp % 60;

      tmp = Math.floor((tmp-min)/60);
      var hour = tmp % 24;

      return ''+('0'+hour).slice(-2)+':'+('0'+min).slice(-2)+':'+('0'+sec).slice(-2);
  }
}
