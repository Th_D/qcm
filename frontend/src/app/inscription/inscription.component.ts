import { Component, OnInit, Input } from '@angular/core';
import { User } from "../model/user";
import { UserService } from "../services/user.service";
import {NgForm} from '@angular/forms';
import { Router } from "@angular/router";

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  identifiant:string;
  password:string;
  nom:string;
  prenom:string;
  repassword:string;

  constructor(private authService: UserService, private router:Router) { }

  ngOnInit() {
  }
  onSubmitInscription(formLogin:NgForm)
  {

    if (this.password == this.repassword && (this.nom!=undefined && this.prenom!=undefined && this.identifiant!=undefined && this.password!=undefined && this.repassword!=undefined)){
      //OK
      this.authService.inscription(this.identifiant, this.password, this.nom, this.prenom).subscribe(
      (result) => {
        console.log("Inscription réussie : " + JSON.stringify(result));
        if(result["message"]=="User already exist")
        {
          //KO    
          console.log(JSON.stringify(result["message"]));      
        }
        else{
          this.router.navigate(['login']);
        }
      },
      (err) => {
        console.log("error : " + err);
      }
    );
    }else{
      //KO
      console.log("L'inscription est incomplète")
    } 
  }
  
}
