import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestQCMViewComponent } from './gest-qcmview.component';

describe('GestQCMViewComponent', () => {
  let component: GestQCMViewComponent;
  let fixture: ComponentFixture<GestQCMViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestQCMViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestQCMViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
