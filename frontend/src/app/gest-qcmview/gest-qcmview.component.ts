import { Component, OnInit } from '@angular/core';
import { QcmService } from '../services/qcm.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gest-qcmview',
  templateUrl: './gest-qcmview.component.html',
  styleUrls: ['./gest-qcmview.component.css']
})
export class GestQCMViewComponent implements OnInit {

  public qcmList = [];
  constructor(private qcmService:QcmService, private router:Router) {
    this.qcmService.getQCMList().subscribe(
      (qcmListFromAPI) => {
        console.log(qcmListFromAPI);
        this.qcmList = qcmListFromAPI['qcm'];
      },
      (err) => {
        console.log(err);
      });
  }

  ngOnInit() {
  }

  goToDetailsQCM()
  {
    this.router.navigate(['professor/gestionQCM/creation']);
  }

  goBack()
  {
    this.router.navigate(['professor']);
  }

}
