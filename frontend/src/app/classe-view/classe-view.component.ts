import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ClasseService } from "../services/classe.service";
@Component({
  selector: 'app-classe-view',
  templateUrl: './classe-view.component.html',
  styleUrls: ['./classe-view.component.css']
})
export class ClasseViewComponent implements OnInit {
  @Input() nom: string;
  @Input() prenom: number;
  public elevesList = [];
  public elevesList2 = [];
  @Input() idClasse: string;
  @Input() idUser: number;

  constructor(private route: ActivatedRoute, private router: Router, private classeService: ClasseService) {
    const id = this.route.snapshot.paramMap.get('id');
    this.idClasse = id;
    //this.idUser = 2;

    this.subscriptions(this.classeService, this.idClasse);
  }

  subscriptions(classeService: ClasseService, idClasse: string) {
    this.classeService.getElevesInClasse(this.idClasse).subscribe(
      (elevesListFromAPI) => {
        this.elevesList = elevesListFromAPI['users'];
      },
      (err) => {
        console.log(err);
      }
    );
    this.classeService.getElevesOutClasse(this.idClasse).subscribe(
      (elevesListFromAPI) => {
        this.elevesList2 = elevesListFromAPI['users'];
      },
      (err) => {
        console.log(err);
      }
    );

    setTimeout(function (classeService, idClasse) {
      this.subscriptions(this.classeService, this.idClasse);
    }.bind(this), 1000);
  }

  ngOnInit() { }





}
