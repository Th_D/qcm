import { Component, OnInit, Input } from '@angular/core';
import { ClasseService } from "../services/classe.service";

@Component({
  selector: 'app-eleve',
  templateUrl: './eleve.component.html',
  styleUrls: ['./eleve.component.css']
})
export class EleveComponent implements OnInit {
  @Input() nom: string;
  @Input() prenom: number;
  @Input() idUser: string;
  @Input() idClasse: string;

  constructor(private classeService: ClasseService) { }

  ngOnInit() {
  }


  removeEleve(idClasse: number, idUser: number) {
    console.log("idClasse : " + idClasse + "  idUser : " + idUser);
    this.classeService.removeEleveInClasse(idUser, idClasse).subscribe(
      (result) => {
        console.log("User " + idUser + " remove from classe " + idClasse)
      },
      (err) => {
        console.log(err);
      }
    );
  }

}
