import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorTaskViewComponent } from './professor-task-view.component';

describe('ProfessorTaskViewComponent', () => {
  let component: ProfessorTaskViewComponent;
  let fixture: ComponentFixture<ProfessorTaskViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorTaskViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorTaskViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
