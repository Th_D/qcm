import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { ClasseService } from '../services/classe.service';

@Component({
  selector: 'app-professor-task-view',
  templateUrl: './professor-task-view.component.html',
  styleUrls: ['./professor-task-view.component.css']
})
export class ProfessorTaskViewComponent implements OnInit {

  profname: string;

  constructor(private userService: UserService, private classService: ClasseService, private router: Router) { }

  ngOnInit() {

  }

  goTo(route: string) {
    this.router.navigate([route]);
  }
}
