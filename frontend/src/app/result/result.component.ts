import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  @Input() prenom:string;
  @Input() nom:string;
  @Input() note:string;

  constructor() { }

  ngOnInit() {
  }

}
