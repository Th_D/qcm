import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class EleveService {
  idUser:number;

  constructor(private http: HttpClient) { }

  getQcmsDispo(date:string) {
    return this.http.get('http://localhost:3000/epreuves/user?now='+date+'&user='+this.idUser);
  }

  getQuestions(idExam:string) {
    return this.http.get('http://localhost:3000/qcm/full?exam='+idExam);
  }

  public setIdUser(idUser:number) {
    this.idUser = idUser;
  }

}
