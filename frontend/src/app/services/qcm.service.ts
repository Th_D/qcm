import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QcmService {

  constructor(private http:HttpClient) {
  }

  getQCMList()
  {
    return this.http.get('http://localhost:3000/qcm');
  }

  addQcm(nom : string, desc : string)
  {
    return this.http.get('http://localhost:3000/qcm/create/?nom='+nom+'&description='+desc);
  }

  addQuestionToQcm(idQcm : number, question:string)
  {
      return this.http.get('http://localhost:3000/qcm/addQuestion?idQcm='+idQcm+'&question='+question);
  }

  addChoiceToQuestion(idQcm:number, idQuestion:number, choiceText:string, isValid:number)
  {
    return this.http.get('http://localhost:3000/qcm/addChoice/?idQcm='+idQcm+'&idQuestion='+idQuestion+'&text='+choiceText+'&good='+isValid);
  }

  addAnswer(idExam:string, idQuestion:string, idChoice:string, idUser:string)
  {
    return this.http.get('http://localhost:3000/epreuves/addAnswer?exam='+idExam+'&question='+idQuestion+'&choice='+idChoice+'&user='+idUser);
  }
}
