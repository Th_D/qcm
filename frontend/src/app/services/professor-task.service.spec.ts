import { TestBed } from '@angular/core/testing';

import { ProfessorTaskService } from './professor-task.service';

describe('ProfessorTaskService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProfessorTaskService = TestBed.get(ProfessorTaskService);
    expect(service).toBeTruthy();
  });
});
