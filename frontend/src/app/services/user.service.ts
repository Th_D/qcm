import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // J'injecte le client HTTP dans le constructeur
  constructor(private http: HttpClient) { }

  // Exemple basique de requête GET
  login(username: string, password: string) {
    return this.http.get('http://localhost:3000/user/connection/?mail=' + username + '&pwd=' + password);
  }

  inscription(mail: string, password: string, nom: string, prenom: string){
    return this.http.get('http://localhost:3000/user/subscription/?nom=' + nom + '&prenom=' + prenom + '&mail=' + mail + '&pwd=' + password);
  }

  // getUsername()
  // {
  //   return this.username;
  // }

}
