import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EpreuvesService {

  constructor(private http: HttpClient) { }

  launchEpreuve(idQcm: number, idClass:number, deb:string, end:string){
      return this.http.get('http://localhost:3000/epreuves/create/?idQcm=' + idQcm + '&idClass=' + idClass + '&deb=' + deb + '&end=' + end);
  }

  getEpreuvesNow(date:string){
      return this.http.get('http://localhost:3000/epreuves/current/?now=' + date);
  }

  getEpreuvesPast(date:string){
      return this.http.get('http://localhost:3000/epreuves/past/?now=' + date);
  }

  getResult(idExam:string){
      return this.http.get('http://localhost:3000/epreuves/result/?idExam=' + idExam);
  }

  getNbQuestions(idExam:string){
      return this.http.get('http://localhost:3000/epreuves/nbQ/?idExam=' + idExam);
  }
}
