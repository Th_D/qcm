import { TestBed } from '@angular/core/testing';

import { EpreuvesService } from './epreuves.service';

describe('EpreuvesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EpreuvesService = TestBed.get(EpreuvesService);
    expect(service).toBeTruthy();
  });
});
