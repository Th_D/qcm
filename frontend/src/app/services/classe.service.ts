import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ClasseService {

  constructor(private http: HttpClient) { }

  // Exemple basique de requête GET
  getClasses() {
    return this.http.get('http://localhost:3000/classes');
  }
  
  addClasse(name:string) {
    return this.http.get('http://localhost:3000/class/create?name='+name);
  }

  removeClasse(id:number) {
    return this.http.get('http://localhost:3000/class/remove?class='+id);
  }
  
  getElevesInClasse(id:string) {
    return this.http.get('http://localhost:3000/class/getUsers?class='+id);
  }
  getElevesOutClasse(id:string) {
    return this.http.get('http://localhost:3000/class/getNotUsers?class='+id);
  }


  addEleveInClasse(id_user:number, id_class:number) {
    return this.http.get('http://localhost:3000/class/addUser?user='+id_user+'&class='+id_class);
  }


  removeEleveInClasse(id_user:number, id_class:number) {
    return this.http.get('http://localhost:3000/class/removeUser?class='+id_class+'&user='+id_user);
  }



  
} 
