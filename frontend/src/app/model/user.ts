export class User
{
  id:number;
  username:string;
  password:string;
  firstName:string;
  lastName:string;
  status:string;
  token:string;

  getUsername()
  {
    return this.username;
  }

  User(username:string, password:string ){
    this.username = username;
    this.password = password;
  }
}
