import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ClasseService } from "../services/classe.service";

@Component({
  selector: 'app-classe',
  templateUrl: './classe.component.html',
  styleUrls: ['./classe.component.css']
})
export class ClasseComponent implements OnInit {

  @Input() name: string;
  @Input() idClasse: number;
  @Input() idUser: number;
  

  constructor(private router: Router, private classeService: ClasseService) { }

  ngOnInit() { }

  removeClasse(id: number): void {
    console.log("id = " + id);
    this.classeService.removeClasse(id).subscribe(
      (result) => {
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addElevesInClasse(id: number): void {
    console.log("id = " + id);
    this.router.navigate(['professor/gestionClasses', id]);
  }
}
