import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { UserService } from './services/user.service';
import { ProfessorTaskService } from './services/professor-task.service';

import { ProfessorTaskComponent } from './professor-task/professor-task.component';
import { ProfessorTaskViewComponent } from './professor-task-view/professor-task-view.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { GestQCMViewComponent } from './gest-qcmview/gest-qcmview.component';
import { QcmComponent } from './qcm/qcm.component';
import { EpreuvesViewComponent } from './epreuves-view/epreuves-view.component';
import { GestElevesviewComponent } from './gest-elevesview/gest-elevesview.component';
import { ClasseComponent } from './classe/classe.component';
import { ClasseViewComponent } from './classe-view/classe-view.component';
import { EleveComponent } from './eleve/eleve.component';
import { CreationQCMViewComponent } from './creation-qcmview/creation-qcmview.component';
import { EpreuvesComponent } from './epreuves/epreuves.component';
import { Eleve2Component } from './eleve2/eleve2.component';
import { ResultComponent } from './result/result.component';
import { EleveTaskComponent } from './eleve-task/eleve-task.component';
import { EleveTaskViewComponent } from './eleve-task-view/eleve-task-view.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalComponent } from './modal/modal.component';
import { QuestionComponent } from './question/question.component';
import { ModalAnswerComponent } from './modal-answer/modal-answer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfessorTaskComponent,
    ProfessorTaskViewComponent,
    InscriptionComponent,
    GestQCMViewComponent,
    QcmComponent,
    EpreuvesViewComponent,
    GestElevesviewComponent,
    ClasseComponent,
    EpreuvesComponent,
    ResultComponent,
    ClasseViewComponent,
    EleveComponent,
    CreationQCMViewComponent,
    ModalComponent,
    EpreuvesComponent,
    Eleve2Component,
    CreationQCMViewComponent,
    EleveTaskComponent,
    EleveTaskViewComponent,
    QuestionComponent,
    ModalAnswerComponent
  ],
  entryComponents: [ModalComponent, ModalAnswerComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  providers: [UserService, ProfessorTaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
