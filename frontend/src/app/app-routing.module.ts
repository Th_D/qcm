import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import {RouterModule, Routes } from "@angular/router";
import { LoginComponent } from './login/login.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ProfessorTaskViewComponent } from './professor-task-view/professor-task-view.component';
import { GestQCMViewComponent } from './gest-qcmview/gest-qcmview.component';
import { EpreuvesViewComponent } from './epreuves-view/epreuves-view.component';
import { GestElevesviewComponent } from './gest-elevesview/gest-elevesview.component';
import { ClasseViewComponent } from './classe-view/classe-view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CreationQCMViewComponent } from './creation-qcmview/creation-qcmview.component';
import { EleveTaskViewComponent } from './eleve-task-view/eleve-task-view.component';


import { HttpClientModule } from '@angular/common/http';

const routes:Routes = [
  {path:'', redirectTo:'login', pathMatch:'full'},
  {path:'login', component: LoginComponent},
  {path:'inscription', component: InscriptionComponent},
  {path:'professor', component:ProfessorTaskViewComponent},
  {path:'professor/gestionQCM', component: GestQCMViewComponent},
  {path:'professor/epreuves', component:EpreuvesViewComponent},
  {path:'professor/gestionEleves', component:GestElevesviewComponent},
  {path:'professor/gestionClasses/:id', component:ClasseViewComponent},
  {path:'professor/gestionQCM/creation',component:CreationQCMViewComponent},
  {path:'eleve', component:EleveTaskViewComponent},
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    RouterModule.forRoot(routes),
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
