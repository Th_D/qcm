import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Eleve2Component } from './eleve2.component';

describe('Eleve2Component', () => {
  let component: Eleve2Component;
  let fixture: ComponentFixture<Eleve2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Eleve2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Eleve2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
