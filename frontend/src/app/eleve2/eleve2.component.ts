import { Component, OnInit, Input } from '@angular/core';
import { ClasseService } from "../services/classe.service";
@Component({
  selector: 'app-eleve2',
  templateUrl: './eleve2.component.html',
  styleUrls: ['./eleve2.component.css']
})
export class Eleve2Component implements OnInit {
  @Input() nom:string;
  @Input() prenom:number;
  @Input() idUser:number;
  @Input() idClasse:number;
  
  

  constructor(private classeService: ClasseService) { }

  ngOnInit() {
  }

  addEleve(idClasse:number,idUser:number){
    console.log("idClasse : "+idClasse + "  idUser : "+ idUser);
    this.classeService.addEleveInClasse(idUser,idClasse).subscribe(
      (result) => {
        console.log("User "+idUser+" add to classe "+idClasse)
      },
      (err) => {
        console.log(err);
      }
    );

  }


  
}
