import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorTaskComponent } from './professor-task.component';

describe('ProfessorTaskComponent', () => {
  let component: ProfessorTaskComponent;
  let fixture: ComponentFixture<ProfessorTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
