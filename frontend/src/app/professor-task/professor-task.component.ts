import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-professor-task',
  templateUrl: './professor-task.component.html',
  styleUrls: ['./professor-task.component.css']
})
export class ProfessorTaskComponent implements OnInit {

  @Input() profTaskName:string;
  @Input() profTaskDesc:string;

  constructor(private router:Router) { }

  ngOnInit() {
  }

  

}
