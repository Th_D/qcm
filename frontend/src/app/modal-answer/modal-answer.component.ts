import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { QcmService } from '../services/qcm.service';
import { FormBuilder, FormGroup, FormArray, FormsModule, ReactiveFormsModule, NgForm } from '@angular/forms';


@Component({
selector: 'modal',
template: `
  <div class="modal-header">
    <h4 class="modal-title pull-left">{{title}}</h4>
    <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <form (ngSubmit)="submit(f)" #f="ngForm">
        <div *ngFor="let question of questionsList">
            <div class="qcmElement">
              <div class="group groupQuestion">
                <label>{{question.enonce}}</label>
              </div>
              <div class='.containerRC'>
                <div class="reponseContainer">
                  <label class ="reponse" type="text">{{question.choix[0].choice}}</label>
                    <input class ="choice" name="cb1" type="checkbox" ngModel>
                  <label class ="reponse" type="text">{{question.choix[1].choice}}</label>
                    <input class ="choice" name="cb2" type="checkbox" ngModel>
                  <label class ="reponse" type="text">{{question.choix[2].choice}}</label>
                    <input class ="choice" name="cb3" type="checkbox" ngModel>
                </div>
              </div>
            </div>
        </div>
        <button type="submit" class="btn btn-default" (click)="bsModalRef.hide()">{{closeBtnName}}</button>
      </form>
  </div>
`
})

export class ModalAnswerComponent implements OnInit {
title: string;
closeBtnName: string;
questionsList = [];

idExam:string;
idQuestion:string;
idChoice:string;
idUser:string;

constructor(private formBuilder: FormBuilder, private qcmService:QcmService, public bsModalRef: BsModalRef) {}

ngOnInit() {
}

submit(form: NgForm){
    //this.idQuestion=form.value['questionId'];
    this.idQuestion="1";
    this.idChoice="1";

    this.qcmService.addAnswer(this.idExam, this.idQuestion, this.idChoice, this.idUser).subscribe(
      (result) => {
        console.log(JSON.stringify(result));
      },
      (err) => {
        console.log(err);
      }
  );
}

}
