import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-epreuves',
  templateUrl: './epreuves.component.html',
  styleUrls: ['./epreuves.component.css']
})
export class EpreuvesComponent implements OnInit {

  @Input() qcm:string;
  @Input() classe:string;
  @Input() date:string;

  constructor() { }

  ngOnInit() {
  }

}
