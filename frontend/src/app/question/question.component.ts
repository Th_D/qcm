import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  @Input() enonce:string;
  @Input() choix1:string;
  @Input() choix2:string;
  @Input() choix3:string;
  constructor() { }

  ngOnInit() {
  }

  submit(){

  }

}
