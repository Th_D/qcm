import { Component, OnInit, Input } from '@angular/core';
import { User } from "../model/user";
import { UserService } from "../services/user.service";
import { NgForm } from '@angular/forms';
import { Router } from "@angular/router";
import { EleveService } from "../services/eleve.service";
import { element } from '@angular/core/src/render3';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  identifiant:string;
  password:string;
  //idUser:string;


  constructor(private userService: UserService, private router:Router, private toto:EleveService) { }
  @Input() idUser:string;

  ngOnInit() {
  }

  onSubmitConnexion(formLogin:NgForm)
  {

    this.userService.login(this.identifiant, this.password).subscribe(
      (result) => {
        //console.log("result : " + JSON.stringify(result));
        this.idUser = result["status"]['idUser'];
        //console.log("idUser ==== "+ this.idUser);
        if(result["status"]['status']=="2")
        {
          this.router.navigate(['professor']);
        }
        if(result["status"]['status']=="1")
        {
          this.router.navigate(['eleve']);
          this.toto.setIdUser(result["status"]['idUser']);
        }
        
      },
      (err) => {
        console.log("error : " + err);
      }
    );
  }

}
