import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
selector: 'modal',
template: `
  <div class="modal-header">
    <h4 class="modal-title pull-left">{{title}}</h4>
    <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
      <div class="row" id="rowQcm">
          <div class="col-xs-12">
            <ul class="list-group">
              <app-result *ngFor="let result of resultsList"
                            [nom] = "result.nom"
                            [prenom] = "result.prenom"
                            [note] = "result.note"></app-result>
            </ul>
          </div>
      </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" (click)="bsModalRef.hide()">{{closeBtnName}}</button>
  </div>
`
})

export class ModalComponent implements OnInit {
title: string;
closeBtnName: string;
resultsList = [];

constructor(public bsModalRef: BsModalRef) {}

ngOnInit() {
}
}
