import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EleveTaskComponent } from './eleve-task.component';

describe('EleveTaskComponent', () => {
  let component: EleveTaskComponent;
  let fixture: ComponentFixture<EleveTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EleveTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EleveTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
