import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-eleve-task',
  templateUrl: './eleve-task.component.html',
  styleUrls: ['./eleve-task.component.css']
})
export class EleveTaskComponent implements OnInit {
  @Input() name: string;
  @Input() desc: string;
  @Input() date: string;
  constructor() { }

  ngOnInit() {
  }

}
