import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpreuvesViewComponent } from './epreuves-view.component';

describe('EpreuvesViewComponent', () => {
  let component: EpreuvesViewComponent;
  let fixture: ComponentFixture<EpreuvesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpreuvesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpreuvesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
