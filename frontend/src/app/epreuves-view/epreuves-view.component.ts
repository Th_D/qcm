import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EpreuvesService } from "../services/epreuves.service";
import { QcmService } from '../services/qcm.service';
import { ClasseService } from "../services/classe.service";
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalComponent } from "../modal/modal.component";

@Component({
  selector: 'app-epreuves-view',
  templateUrl: './epreuves-view.component.html',
  styleUrls: ['./epreuves-view.component.css']
})
export class EpreuvesViewComponent implements OnInit {

  bsModalRef: BsModalRef;

  idQcm:number;
  idClass:number;
  duree:number;

  public qcmList = [];
  public classesList = [];

  public epreuvesNowList = [];
  public epreuvesPastList = [];

  public resultsList = [];

  constructor(private epreuvesService: EpreuvesService, private qcmService: QcmService, private classeService: ClasseService, private router: Router, private modalService: BsModalService) {
      this.classeService.getClasses().subscribe(
        (classesListFromAPI) => {
          this.classesList = classesListFromAPI['classes'];

        },
        (err) => {
          console.log(err);
        }
      );

      this.qcmService.getQCMList().subscribe(
        (qcmListFromAPI) => {
          this.qcmList = qcmListFromAPI['qcm'];
        },
        (err) => {
          console.log(err);
        }
      );

      this.epreuvesServiceSubscription(this.epreuvesService);
  }

  openModal(qcm:string, classe:string, date:string, idExam:string) {
    this.getResult(idExam, function(){
        const initialState = {
          title: ''+qcm+' | '+classe+' | '+date,
          resultsList: this.resultsList
        };
        this.bsModalRef = this.modalService.show(ModalComponent, {initialState});
        this.bsModalRef.content.closeBtnName = 'Close';
    }.bind(this));


  }

  epreuvesServiceSubscription(epreuvesService:EpreuvesService){
      this.epreuvesService.getEpreuvesNow(this.toTimestamp(new Date())).subscribe(
        (epreuvesNowListFromAPI) => {
            this.epreuvesNowList = epreuvesNowListFromAPI['epreuves'];
        },
        (err) => {
            console.log(err);
        }
      );

      this.epreuvesService.getEpreuvesPast(this.toTimestamp(new Date())).subscribe(
        (epreuvesPastListFromAPI) => {
            this.epreuvesPastList = epreuvesPastListFromAPI['epreuves'];
        },
        (err) => {
            console.log(err);
        }
      );

      setTimeout(function(epreuvesService){
          this.epreuvesServiceSubscription(this.epreuvesService);
      }.bind(this), 1000);
  }

  ngOnInit() {
  }

  getResult(idExam:string, callback){
      this.epreuvesService.getResult(idExam).subscribe(
        (resultsListFromAPI) => {
            this.resultsList = resultsListFromAPI['result'];
            this.epreuvesService.getNbQuestions(idExam).subscribe(
                (result) => {
                  for(let personne of this.resultsList){
                      personne.note = (+personne.note/result['nbQ'][0]['nbQ'])*20;
                  }
                  callback();
                },
                (err) => {
                  console.log("error : " + err);
                }
            );
        },
        (err) => {
          console.log("error : " + err);
        }
      );
  }

  onSubmitEpreuve(formLogin:NgForm){
      var deb = this.toTimestamp(new Date());
      var end = this.toTimestamp(this.creerEndDate(new Date(),this.duree));

      this.epreuvesService.launchEpreuve(this.idQcm, this.idClass, deb, end).subscribe(
        (result) => {
          console.log("result : " + JSON.stringify(result));
        },
        (err) => {
          console.log("error : " + err);
        }
      );
  }

  creerEndDate(deb:Date, duree:number){
      return new Date(new Date().getTime() + duree*60000);
  }

  toTimestamp(date:Date){
      var annee   = date.getFullYear();
      var mois    = date.getMonth() + 1;
      var jour    = date.getDate();
      var heure   = date.getHours();
      var minute  = date.getMinutes();
      var seconde = date.getSeconds();
      return ''+annee+'-'+mois+'-'+jour+' '+heure+':'+minute+':'+seconde;
  }

  toDate(dateT:string){
      var date = new Date(dateT);
      var annee   = date.getFullYear();
      var mois    = ('0'+(date.getMonth() + 1)).slice(-2);
      var jour    = ('0'+date.getDate()).slice(-2);
      var heure   = ('0'+date.getHours()).slice(-2);
      var minute  = ('0'+date.getMinutes()).slice(-2);
      var seconde = ('0'+date.getSeconds()).slice(-2);
      return ''+jour+'/'+mois+'/'+annee+' '+heure+':'+minute+':'+seconde;
  }

  toEnd(date:string){
      var now = +new Date();
      var tmp = +new Date(date) - now;

      tmp = Math.floor(tmp/1000);
      var sec = tmp % 60;

      tmp = Math.floor((+tmp-sec)/60);
      var min = tmp % 60;

      tmp = Math.floor((tmp-min)/60);
      var hour = tmp % 24;

      return ''+('0'+hour).slice(-2)+':'+('0'+min).slice(-2)+':'+('0'+sec).slice(-2);
  }
}
