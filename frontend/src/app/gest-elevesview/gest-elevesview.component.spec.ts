import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestElevesviewComponent } from './gest-elevesview.component';

describe('GestElevesviewComponent', () => {
  let component: GestElevesviewComponent;
  let fixture: ComponentFixture<GestElevesviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestElevesviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestElevesviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
