import { Component, OnInit, Input } from '@angular/core';
import { ClasseService } from "../services/classe.service";
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gest-elevesview',
  templateUrl: './gest-elevesview.component.html',
  styleUrls: ['./gest-elevesview.component.css']
})
export class GestElevesviewComponent implements OnInit {
  public classesList = [];
  ValName = new FormControl('');
  nameClasse: string;
  idUser:number;

  constructor(private classeService: ClasseService, public router:Router) {
    this.subscriptions(this.classeService);
  } 

  subscriptions(classeService:ClasseService){
    this.classeService.getClasses().subscribe(
      (classesListFromAPI) => {
        this.classesList = classesListFromAPI['classes'];

      },
      (err) => {
        console.log(err);
      }
    );

    setTimeout(function(classeService){
      this.subscriptions(this.classeService);
    }.bind(this),1000);
  }

  ngOnInit() { }

  addClasse(name: string) {
    console.log("Nom de la classe = " + this.nameClasse);

    this.classeService.addClasse(this.nameClasse).subscribe(
      (result) => {
        console.log("La classe a été créée")
      },
      (err) => {
        console.log(err);
      }
    );

  }



}
