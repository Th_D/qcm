import {Request, Response} from "express";

export class QcmRoutes {

    public routes(app, bd): void {

        //get all QCM
        app.route('/qcm')
        .get((req: Request, res: Response) => {
            bd.getAllQcm(function(qcm){
                res.status(200).send({
                    qcm
                });
            });
        })

        // Qcm creation
        app.route('/qcm/create')
        .get((req: Request, res: Response) => {
            var nom = req.query.nom;
            var description = req.query.description;
            console.log('ZIAK');
            bd.addQcm(nom, description, function(idQcm){
                res.status(200).send({
                    idQcm: idQcm
                });
            });
        })

        // add question to a Qcm
        app.route('/qcm/addQuestion')
        .get((req: Request, res: Response) => {
            var idQcm = req.query.idQcm;
            var question = req.query.question;
            bd.addQuestion(idQcm, question, function(idQuestion){
                res.status(200).send({
                    idQuestion: idQuestion
                });
            });
        })

        //add a choice for a question of a qcm
        app.route('/qcm/addChoice')
        .get((req: Request, res: Response) => {
            var idQcm = req.query.idQcm;
            var idQuestion = req.query.idQuestion;
            var text = req.query.text;
            var good = req.query.good;
            bd.addChoice(idQcm, idQuestion, text, good, function(idChoice ){
                res.status(200).send({
                    idChoice: idChoice
                });
            });
        })

        app.route('/qcm/questions')
        .get((req: Request, res: Response) => {
          var idQcm = req.query.qcm;
          bd.getQuestions(idQcm, function(questions){
              res.status(200).send({
                  questions
              });
          });
        })

        app.route('/qcm/full')
        .get((req: Request, res: Response) => {
          var idExam = req.query.exam;
          bd.getQuestionsAndChoice(idExam, function(questions){
              res.status(200).send({
                  questions
              });
          });
        })

        app.route('/exam/now')
        .get((req: Request, res: Response) => {
          var date = req.query.date;
          bd.getExamNow(date, function(now){
              res.status(200).send({
                  now
              });
          });
        })
    }
}
