import {Request, Response} from "express";

export class ClassRoutes {

    public routes(app, bd): void {


        // get all classes
        app.route('/classes')
        .get((req: Request, res: Response) => {
            bd.getAllClasses(function(classes){
                res.status(200).send({
                    classes : classes
                });
            });
        })

        //create a class
        app.route('/class/create')
        .get((req: Request, res: Response) => {
            var name = req.query.name;
            bd.addClass(name, function(id){
                res.status(200).send({
                    idInsertion: id
                });
            });
        })

        //add a user in a class
        app.route('/class/addUser')
        .get((req: Request, res: Response) => {
            var user = req.query.user;
            var classe = req.query.class;
            bd.addUserInClass(user, classe, function(id){
                res.status(200).send({
                    idInsertion: id
                });
            });
        })

        //remove a user in a class
        app.route('/class/removeUser')
        .get((req: Request, res: Response) => {
            var user = req.query.user;
            var classe = req.query.class;
            bd.removeUserFromClass(user, classe, function(id){
                res.status(200).send({
                    idInsertion: id
                });
            });
        })

        //get all users of a class
        app.route('/class/getUsers')
        .get((req: Request, res: Response) => {
          var classe = req.query.class;
            bd.getClassUsers(classe, function(users){
                res.status(200).send({
                    users
                });
            });
        })

        //remove a specified class
        app.route('/class/getNotUsers')
        .get((req: Request, res: Response) => {
          var classe = req.query.class;
            bd.getClassNotUsers(classe, function(users){
                res.status(200).send({
                    users
                });
            });
        })

        //remove a specified class
        app.route('/class/remove')
        .get((req: Request, res: Response) => {
          var classe = req.query.class;
            bd.removeClass(classe, function(status){
                res.status(200).send({
                    status: status
                });
            });
        })
    }
}
