import {Request, Response} from "express";

export class UserRoutes {

    public routes(app, bd): void {

        // User connection
        app.route('/user/connection')
        .get((req: Request, res: Response) => {
            var mail = req.query.mail;
            var pwd = req.query.pwd;
            bd.checkConnection(mail, pwd, function(status){
                res.status(200).send({
                    status: status
                });
            });
        })

        // User subscription
        app.route('/user/subscription')
        .get((req: Request, res: Response) => {
            var nom = req.query.nom;
            var prenom = req.query.prenom;
            var mail = req.query.mail;
            var pwd = req.query.pwd;
            bd.mailExist(mail, function(exist){
                if(exist){
                    res.status(200).send({
                        message: 'User already exist'
                    });
                }
                else{
                    bd.addUser(mail, pwd, nom, prenom);
                    res.status(200).send({
                        message: 'User add : '+nom+' '+prenom
                    });
                }
            });
        })
    }
}
