import {Request, Response} from "express";

export class EpreuvesRoutes {

    public routes(app, bd): void {

      // launchEpreuve
      app.route('/epreuves/create')
      .get((req: Request, res: Response) => {
          var idQcm = req.query.idQcm;
          var idClass = req.query.idClass;
          var dateStart = req.query.deb;
          var dateEnd = req.query.end;
          bd.addExam(idQcm, idClass, dateStart, dateEnd, function(message){
              res.status(200).send({
                  message: message
              });
          });
      })

      // get all incoming epreuve for a user
      app.route('/epreuves/user')
      .get((req: Request, res: Response) => {
          var idUser = req.query.user;
          var date = req.query.now;
          bd.getCurrentForUser(date, idUser, function(epreuves){
              res.status(200).send({
                  epreuves
              });
          });
      })

      // get all incoming epreuve for a user
      app.route('/epreuves/userPast')
      .get((req: Request, res: Response) => {
          var idUser = req.query.user;
          var date = req.query.now;
          bd.getPastForUser(date, idUser, function(epreuves){
              res.status(200).send({
                  epreuves
              });
          });
      })

      app.route('/epreuves/past')
      .get((req: Request, res: Response) => {
          var date = req.query.now;
          bd.getPastEpreuve(date, function(epreuves){
              res.status(200).send({
                  epreuves
              });
          });
      })

      app.route('/epreuves/current')
      .get((req: Request, res: Response) => {
          var date = req.query.now;
          bd.getCurrentEpreuve(date, function(epreuves){
              res.status(200).send({
                  epreuves
              });
          });
      })

      app.route('/epreuves/result')
      .get((req: Request, res: Response) => {
          var idExam = req.query.idExam;
          bd.getResult(idExam, function(result){
              res.status(200).send({
                  result
              });
          });
      })

      app.route('/epreuves/nbQ')
      .get((req: Request, res: Response) => {
          var idExam = req.query.idExam;
          bd.getNbQuestions(idExam, function(nbQ){
              res.status(200).send({
                  nbQ
              });
          });
      })

      app.route('/epreuves/addAnswer')
      .get((req: Request, res: Response) => {
          var idExam = req.query.exam;
          var idQuestion = req.query.question;
          var idChoice = req.query.choice;
          var idUser = req.query.user;


          bd.addReponse(idExam, idQuestion, idChoice, idUser, function(ok){
              res.status(200).send({
                  ok
              });
          });
      })



    }
}
