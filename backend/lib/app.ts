import * as express from "express";
import * as bodyParser from "body-parser";
import { UserRoutes } from "./routes/userRoutes";
import { QcmRoutes } from "./routes/qcmRoutes";
import { EpreuvesRoutes } from "./routes/epreuvesRoutes";
import { ClassRoutes } from "./routes/classRoutes";
import { Bd } from "./bd";

class App {

    public app: express.Application;
    public userRoutes: UserRoutes = new UserRoutes();
    public qcmRoutes: QcmRoutes = new QcmRoutes();
    public epreuvesRoutes: EpreuvesRoutes = new EpreuvesRoutes();
    public classRoutes: ClassRoutes = new ClassRoutes();
    public bd: Bd = new Bd();

    constructor() {
        this.app = express();
        this.config();
        this.userRoutes.routes(this.app, this.bd);
        this.qcmRoutes.routes(this.app, this.bd);
        this.epreuvesRoutes.routes(this.app, this.bd);
        this.classRoutes.routes(this.app, this.bd);

        // add a user
        //this.bd.addUser("test", "mdptest");

        // add a manager
        //this.bd.addManager("test", "mdptest");

        //check connexion
        //console.log(this.bd.checkConnection("titi", "pwd"));


        //this.bd.checkConnection('dodo', 'pwdodo');
    }


    private config(): void{
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));

        this.app.use(function(req, res, next) {
          res.header("Access-Control-Allow-Origin", "*");
          res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
          next();
        });

    }

}

export default new App().app;
