export class Bd {

  private con;

  constructor(){
    console.log("bd start!");

    var mysql = require('mysql');
    this.con = this.connexion(mysql);
    //this.testreq();
  }

/* ========================
 *=======PRIVATE=========
 =======================*/
  private connexion(mysql){
    var con = mysql.createConnection({
      host: "remotemysql.com",
      user: "enmTP3HcDJ",
      password: "88T5TpMB4c",
      port: "3306",
      database: "enmTP3HcDJ"
    });


    con.connect(function(err){
      if (err) throw err;
    });


    return con;
  }

  // private testreq(){
  //   this.con.connect(function(err){
  //     if (err) throw err;
  //     this.con.query("SELECT * FROM user;", function (err, result) {
  //       if (err) throw err;
  //       console.log(result);
  //     });
  //   }.bind(this));
  // }

  //INSERT INTO user (mail, password, status)
  //VALUES ('romzer', 'mdpRomzerr', 1)
  private exec_req(req){
    this.con.query(req, function (err, result) {
      if (err) throw err;
      //console.log(result);
    });
  }

  private get_exec_req(req, fct){
    this.con.query(req, function (err, result) {
      if (err) throw err;
      //console.log(result);
      fct(result);
    });
  }

  /* ========================
   *=======PUBLIC==========
   =======================*/
// INSERT
   public addUser(mail, pwd, nom, prenom){
     this.exec_req("INSERT INTO user (mail, password, status, nom, prenom) VALUES ('"+mail+"', '"+pwd+"', 1, '"+nom+"', '"+prenom+"')");
   }

   public addManager(mail, pwd, nom, prenom){
     this.exec_req("INSERT INTO user (mail, password, status, nom, prenom) VALUES ('"+mail+"', '"+pwd+"', 2, '"+nom+"', '"+prenom+"')");
   }

   public addQcm(name, text, callback){
     this.con.query("INSERT INTO qcm (name, text) VALUES ('"+name+"', '"+text+"')", function(err, result) {
       if (err) throw err;
       callback(result.insertId);
     });
   }

   public addClass(name, callback){
     this.con.query("INSERT INTO class (name) VALUES ('"+name+"')", function(err, result) {
       if (err) throw err;
       callback(result.insertId);
     });
   }

   public addUserInClass(idUser, idClass, callback){
     this.con.query("INSERT INTO classUser (idUser, idClass) VALUES ('"+idUser+"', '"+idClass+"')", function(err, result) {
       if (err) throw err;
       callback(result.insertId);
     });
   }

   public removeUserFromClass(idUser, idClass, callback){
     this.con.query("DELETE FROM classUser WHERE idUser = '"+idUser+"' AND idClass = '"+idClass+"'", function(err, result) {
       if (err) throw err;
       callback(result.insertId);
     });
   }

   public addQuestion(qcm, text, callback){
     this.con.query("INSERT INTO question (idQcm, text) VALUES ("+qcm+", '"+text+"')", function(err, result) {
       if (err) throw err;
       callback(result.insertId);
     });
   }

   public addChoice(qcm, question, text, good, callback){
     this.con.query("INSERT INTO choice (idQcm, idQuestion, text, good) VALUES ("+qcm+", '"+question+"', '"+text+"', "+good+")", function(err, result) {
       if (err) throw err;
       callback(result.insertId);
     });
   }

   public getAllClasses(callback){
     this.con.query("SELECT * FROM class", function(err, result) {
       if (err) throw err;
       callback(result);
     });
   }

   public getClassUsers(classe, callback){
     console.log(classe);
     this.con.query("SELECT * FROM user, classUser WHERE user.idUser = classUser.idUser AND classUser.idClass =  "+classe+" ORDER BY user.nom", function(err, result) {
       if (err) throw err;
       callback(result);
     });
   }

   public getClassNotUsers(classe, callback){
     this.con.query("SELECT * FROM user WHERE user.idUser NOT IN (\
                        SELECT user.idUser FROM user, classUser WHERE user.idUser = classUser.idUser AND classUser.idClass = "+classe+") ORDER BY user.nom",
                     function(err, result) {
       if (err) throw err;
       callback(result);
     });
   }

   //EXAM
   public addExam(idQcm, idClass, dateStart, dateEnd, callback){
     this.con.query("INSERT INTO exam (idQcm, idClass, dateStart, dateEnd) VALUES ('"+idQcm+"', '"+idClass+"', '"+dateStart+"', '"+dateEnd+"')", function(err, result) {
       if(err){
           callback('Error lors du lancement de l\'épreuve')
           throw err;
       }
       else{
           callback ('Examen lancé', result);
       }
     });
   }
   public getAllQcm(callback){
     this.con.query("SELECT * FROM qcm", function(err, result) {
       if (err) throw err;
       callback(result);
     });
   }

  // SELECT
  public mailExist(mail, callback){
    var sql = "SELECT count(*) AS res FROM user WHERE mail = '"+mail+"' ";
    this.get_exec_req(sql, function(r){callback(r[0]['res']==1);});
  }

  //return 0 if user doesn't exist
  //       1 for student
  //       2 for teacher
  public checkConnection(mail, pwd, callback){
    var sql = "SELECT * FROM user WHERE mail = '"+mail+"' AND password = '"+pwd+"'";
    this.get_exec_req(sql, function(r){callback((r.length!=1)?0:r[0]);});
  }


  public getQuestions(idQcm, callback){
    this.con.query("SELECT * FROM question WHERE idQcm = '"+ idQcm +"'", function(err, result) {
      if (err) throw err;
      callback(result);
    });
  }

  public getQuestionsAndChoice(idExam, callback){
    this.con.query("SELECT E.idExam, Q.idQuestion, C.idChoice, C.text as choice, Q.text as enonce \
                    FROM question Q, \
                    choice C, exam E \
                    WHERE Q.idQuestion = C.idQuestion \
                    AND Q.idQcm = C.idQcm \
                    AND Q.idQcm = E.idQcm \
                    AND E.idExam = '"+ idExam +"'",
                    function(err, result) {
      if (err) throw err;
      callback(result);
    });
  }

    public getExamNow(date, callback){
    this.con.query("SELECT exam.idExam, qcm.name AS QCM, class.name AS CLASS, exam.dateEnd \
                    FROM exam, qcm, class \
                    WHERE exam.idQcm = qcm.idQcm\
                    AND class.idClasse = exam.idClass \
                    AND dateEnd > '"+date+"'",
                    function(err, result) {
      if (err) throw err;
      callback(result);
    });
  }

  public getCurrentForUser(date, idUser, callback){
    this.con.query("SELECT exam.idExam, classUser.idUser, qcm.name AS QCM, class.name AS CLASS, exam.dateEnd, qcm.text AS DESCR\
                    FROM exam, qcm, class, classUser \
                    WHERE exam.idQcm = qcm.idQcm \
                    AND class.idClasse = exam.idClass \
                    AND classUser.idClass = class.idClasse \
                    AND classUser.idUser = '"+ idUser+"' \
                    AND dateEnd > '"+date+"'",
                    function(err, result) {
      if (err) throw err;
      callback(result);
    });
  }

  public getPastForUser(date, idUser, callback){
    this.con.query("SELECT exam.idExam, user.idUser, qcm.name AS QCM, class.name AS CLASS, exam.dateEnd, user.mail \
                    FROM exam, qcm, class, user, classUser \
                    WHERE exam.idQcm = qcm.idQcm \
                    AND class.idClasse = exam.idClass \
                    AND classUser.idUser = user.idUser \
                    AND classUser.idClass = class.idClasse \
                    AND dateEnd < '"+date+"'",
                    function(err, result) {
      if (err) throw err;
      callback(result);
    });
  }

  public removeClass(idClass, callback){
    this.con.query("DELETE FROM class WHERE idClasse ="+idClass,
                    function(err, result) {
      if (err) throw err;
      callback(result);
    });
  }

  public getPastEpreuve(time, callback){
    this.con.query("SELECT exam.idExam, qcm.name AS QCM, class.name AS CLASS, exam.dateEnd \
                    FROM exam, qcm, class \
                    WHERE exam.idQcm = qcm.idQcm \
                    AND class.idClasse = exam.idClass \
                    AND dateEnd < '"+time+"' \
                    ORDER BY exam.dateEnd DESC",
                    function(err, result) {
      if (err) throw err;
      callback(result);
    });
  }

  public getCurrentEpreuve(time, callback){
    this.con.query("SELECT exam.idExam, qcm.name AS QCM, class.name AS CLASS, exam.dateEnd \
                    FROM exam, qcm, class \
                    WHERE exam.idQcm = qcm.idQcm \
                    AND class.idClasse = exam.idClass \
                    AND dateEnd > '"+time+"' \
                    ORDER BY exam.dateEnd DESC ",
                    function(err, result) {
      if (err) throw err;
      callback(result);
    });
  }

  public getResult(idExam, callback){
    this.con.query("SELECT user.nom, user.prenom, count(*) AS note \
                    FROM exam, user, question, choice, reponse \
                    WHERE exam.idExam = '"+idExam+"' \
                    AND exam.idQcm = question.idQcm \
                    AND question.idQuestion = choice.idQuestion \
                    AND question.idQcm = choice.idQcm \
                    AND choice.idChoice = reponse.idChoice \
                    AND choice.good = 1 \
                    AND reponse.idExam = exam.idExam \
                    AND reponse.idUser = user.idUser \
                    GROUP BY reponse.idUser",
                    function(err, result) {
      if (err) throw err;
      callback(result);
    });
  }

  public getNbQuestions(idExam, callback){
    this.con.query("SELECT count(*) as nbQ \
                  FROM exam, question \
                  WHERE exam.idExam = '"+idExam+"' \
                  AND exam.idQcm = question.idQcm",
                  function(err, result) {
    if (err) throw err;
    callback(result);
    });
  }

  public addReponse(idExam, idQuestion, idChoice, idUser, callback){
    this.con.query("INSERT INTO reponse (idExam, idUser, idQuestion, idChoice) VALUES ('"+idExam+"', '"+idUser+"', '"+idQuestion+"', '"+idChoice+"')", function(err, result) {
      if(err){
          callback('Error lors de l\'envoie de la reponse')
          throw err;
      }
      else{
          callback ('Reponse envoyee', result);
      }
    });
  }


}
